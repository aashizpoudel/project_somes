import React, {Component} from 'react';

export default class Footer extends Component{
    
    render(){
        
        return <footer className="bg-dark text-light">
  <div className="container">
    <div className="row">
      <div className="col-md-6 p-4">
        <p>Society of Mechanical Engineering Society</p>
        <p>Dharan-8,Sunsari</p>
      </div>
      <div className="col-md-6 p-4">
        <h4>Quick Links</h4>
        <hr />
        <ul className="">
          <a href="#" className="text-light">
            <li>Dummy</li>
          </a>
          <a href="#" className="text-light">
            <li>Dummy</li>
          </a>
          <a href="#" className="text-light">
            <li>Dummy</li>
          </a>{" "}
          <a href="#" className="text-light">
            <li>Dummy</li>
          </a>
        </ul>
      </div>
    </div>
  </div>
</footer>;


        
    }
    
}