import React, {Component} from 'react';

export default class Header extends Component{
    
    render(){
        
        return <header className="p-5">
  <div className="container">
    <div className="row">
      <div className="col-md-4 logo">
        <img src="/somes.png" width="125" height="125" className="img-responsive  logo-spin" alt="logo" />
      </div>
      <div className="col headertext">
        <h2>Society of Mechanical Engineering Society,Purwanchal Campus Dharan</h2>
      </div>
    </div>
  </div>
</header>;

        
    }
    
}