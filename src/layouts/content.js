import React, {Component} from 'react';
import {Route,Switch} from 'react-router-dom';
import Login from './../pages/login.js';
import Committee from './../pages/committee.js';
import Contact from './../pages/contact.js';
import Events from './../pages/events.js';
import Home from './../pages/home.js';
import Materials from './../pages/materials.js';
import AddMaterial from './../pages/addmaterial.js';
import ClassSchedule from './../pages/class-schedule'
export default class Content extends Component{
    
    render(){
        
        return  <main>
    <div className='container p-4 content'>
      <Switch>
      <Route path="/login"  component={Login} />
      <Route exact path="/" component={Home} />
      <Route path="/events"  component={Events} />
      <Route path="/contacts"  component={Contact} />
      <Route path="/committee"  component={Committee} />
      <Route exact path="/materials"  component={Materials} />
      <Route path="/materials/add"  component={AddMaterial} />
      <Route path="/class-schedules" component={ClassSchedule} />

      <Route component={(props)=><h3 className='text-danger'>Oops!! Really awesome machine at <code className="lead">"{props.location.pathname}"</code> is out of service right now!!... Sorry about that!!</h3>}/>
      </Switch>
    </div>
  </main>

        
    }
    
}