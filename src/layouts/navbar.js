import React, {Component} from 'react';
import {NavLink,Link} from 'react-router-dom';
import SignedInLinks from './../components/links/signedinlinks.js';
import SignedOutLinks from './../components/links/signedoutlinks.js';
import AdminLinks from './../components/links/adminlinks.js';
import {connect} from 'react-redux';
import {signOut} from './../store/actions/authAction';
class NavBar extends Component{
    
    render(){
        console.log(this.props.auth);
        return <nav className="navbar navbar-expand-md navbar-dark bg-primary ">
  <div className="container">
    <Link className="navbar-brand" to="/">
      <i className="fa fa-home" />
    </Link>
    <button
      className="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarNavDropdown"
      aria-controls="navbarNavDropdown"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span className="navbar-toggler-icon" />
    </button>
    <div className="collapse navbar-collapse" id="navbarNavDropdown">
      <ul className="navbar-nav ml-auto">
       <li className='nav-item'>
        <NavLink className="nav-link" to="/">Home</NavLink>
       </li>
       <li className='nav-item'>
        <NavLink className="nav-link" to="/committee">Committee</NavLink>
       </li>
       <li className='nav-item'>
        <NavLink className="nav-link" to="/events">Events</NavLink>
       </li>
                  <li className='nav-item' ><NavLink to="/class-schedules" className="nav-link">Class Schedules</NavLink></li>

       <li className='nav-item'>
        <NavLink className="nav-link" to="/contacts">Contact</NavLink>
       </li>
        
      </ul>
         {this.props.auth.uid === "mSuupRBIYJZvhPhAxndPlYyesDs1"? 
      <AdminLinks />:''
      
      }
      {this.props.auth.isEmpty?
      <SignedOutLinks />
      :
      [
      <SignedInLinks />,
      <ul className='navbar-nav'>
       <li className='nav-item'>
        <a className="nav-link" href="#" onClick={()=>{this.props.signOut()}}>Logout</a>
       </li>
       </ul>
        ]
      }
      
   
      
       

    </div>
  </div>
</nav>;


        
    }
    
}


const mapStateToProps = (state)=>{
    // console.log(state);
    return {
       auth:state.firebase.auth
    }
};

const mapDispatchToProps= (dispatch)=>{
   return {
        signOut:()=>dispatch(signOut()), 
   }
};
export default connect(mapStateToProps,mapDispatchToProps)(NavBar);

