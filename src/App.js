import React, { Component } from 'react';
import Header from './layouts/header.js';
import Navbar from './layouts/navbar.js';
import Footer from './layouts/footer.js';
import Content from './layouts/content.js';
import {BrowserRouter} from 'react-router-dom';

import './App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div className="bg-light text-dark">
        <Header />
        <Navbar />
        <Content />
        <Footer/>
      
      </div>
      </BrowserRouter>
    );
  }
}

export default App;
