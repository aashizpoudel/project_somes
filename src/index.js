import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import thunk from 'redux-thunk';
import {compose,createStore,applyMiddleware} from 'redux';
import rootReducer from './store/reducers/';
import {Provider} from 'react-redux';

import {reactReduxFirebase,getFirebase} from 'react-redux-firebase';
import {reduxFirestore,getFirestore} from 'redux-firestore';
import firebase from './config/fbconfig';

const store=createStore(rootReducer,
compose(
applyMiddleware(thunk.withExtraArgument({getFirebase,getFirestore})),

reduxFirestore(firebase,{allowMultipleListeners:true}),
reactReduxFirebase(firebase,{
    userProfile: 'users',
    useFirestoreForProfile: true,
    attachAuthIsReady:true,
    allowMultipleListeners:true
  }),
)

);
store.firebaseAuthIsReady.then(()=>{
    ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
    serviceWorker.unregister();

});


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
