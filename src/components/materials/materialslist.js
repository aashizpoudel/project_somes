import React, {Component} from 'react';


import {connect} from 'react-redux';
import {compose} from 'redux';
import {firestoreConnect} from 'react-redux-firebase';
class SingleMaterial extends Component{
    render(){
        return <div className='card p-4 mb-3'>
                            <p><strong>Material Name :</strong>{this.props.material.title}</p>
                            <p><strong>Subject Related:</strong> {this.props.material.subject} </p>
                            <p className='form-inline'><input type="url" className=" form-control" readOnly value={this.props.material.url}/>
                                <a href={this.props.material.url} target="_blank" className='btn btn-outline-primary'>Go to this link</a>
                            </p>
                        </div> ;
    }
}

class MaterialList extends Component{

    render(){
        console.log(this.props.status);
        const {materials} = this.props;
        
        if(!materials){
            return <p>loading...</p>
        }
        return <div>
            
        {materials.map((material,i)=>{
            
            return <SingleMaterial key={material.id} material={material}/> ;
        })}
        </div>
    }
    
}



const mapStateToProps = (state)=>{
    return {
        materials: state.firestore.ordered.materials,
        status: state.firestore.status
    }
}
const mapDispatchToProps = (dispatch)=>{
    return null;
}

export default compose(connect(mapStateToProps,mapDispatchToProps),
    firestoreConnect(['materials'])
)(MaterialList);

// export default  MaterialList;