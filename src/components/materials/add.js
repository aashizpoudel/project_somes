import React,{Component} from 'react';
import {createMaterial} from './../../store/actions/materialAction';
import {connect} from 'react-redux';
class MaterialAddForm extends Component{
    
    render(){
        console.log(this.props);
        
        return <div className='row justify-content-center p-2'>
        
        <div className='col-md-6'>
        <h3>Material Add Form</h3>
        <hr/>
                <form onSubmit={(e)=>{
                    // this.props.login({name:"aashiz"});
                e.preventDefault();

                    // this.setState({working:true});
                     this.props.createMaterial({title:e.target.title.value,subject:e.target.subject.value,url:e.target.url.value,semester:e.target.semester.value});
                    this.props.history.push("/materials");
                    
                }}>
                <div className='form-group'><label htmlFor="title" className="form-label">Material Title</label>
                    <input className='form-control' id="title" name="title" required type="name" placeholder="Enter the title of material here" />
                </div><div className='form-group'>
                <label htmlFor="subject" className="form-label">Subject</label>
                    <input id="subject" className='form-control' name="subject" required type="text" placeholder="Enter the subject this material is related to." />
                </div>
                <div className='form-group'>
                <label htmlFor="semester" className="form-label">Semester</label>
                    <input id="semester" className='form-control' name="semester" required type="number" placeholder="Enter the semester" />
                </div>
                <div className='form-group'><label htmlFor="url" className="form-label">Material Link</label>
                    <input className='form-control' id="url" name="url" required type="url" placeholder="Enter link to material" />
                </div>
                <div className='form-group text-right'>
                    <input className='form-control' className='btn btn-primary mr-auto' type="submit" value="Add Material"/>
                </div>
                </form>
        
        </div>
        </div>
    }
}

const mapStateToProps=(state)=>{
    return {
        material : state.materials
    }
}

const mapDispatchToProps=(dispatch)=>{
    return {
        createMaterial :material=> dispatch(createMaterial(material))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(MaterialAddForm);