import React from 'react';
const SinglePerson = ()=> <div className='col-md-4 p-2'><div className="card shadow rounded border">
        <div className="card-img-top">
          <img className="img-responsive" src="https://images.pexels.com/photos/1698349/pexels-photo-1698349.jpeg?auto=compress&cs=tinysrgb&h=300" width="100%" />
        </div>
        <div className="card-body">
          <div className="card-title">
            <h5 className="text-center mt-1">Fungsuk Wangdu</h5>
          </div>
          <div className="card-subtitle">
            <h6 className="text-muted text-center">President</h6>
          </div>
          <div className="card-text">
            <p className="text-center">Lorem ipsum dos nascimento blah blasda asd ad af aj faw ifasnabsfab fahwfw afas f</p>
          </div>
        </div>
      </div>
      </div>
    ;

export default SinglePerson;