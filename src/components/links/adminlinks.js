import React from 'react';
import {NavLink} from 'react-router-dom';

const AdminLinks = ()=> <ul className="navbar-nav">
    <li className='nav-item'><NavLink to="/users" className="nav-link">Users List</NavLink></li>
   </ul>;

export default AdminLinks;