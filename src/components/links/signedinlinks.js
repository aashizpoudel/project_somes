import React from 'react';
import {NavLink} from 'react-router-dom';

const SignedInLinks = ()=> <ul className="navbar-nav">
    <li className='nav-item' to="/books"><NavLink to="/books" className="nav-link">Book Manager</NavLink></li>
    <li className='nav-item' to="/materials"><NavLink to="/materials" className="nav-link">Materials</NavLink></li>
   
  
   </ul>;

export default SignedInLinks;