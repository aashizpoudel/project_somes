import React from 'react';
import {NavLink} from 'react-router-dom';

const SignedOutLinks = ()=> <ul className="navbar-nav">
    <li className='nav-item' ><NavLink to="/login" className="nav-link">Login</NavLink></li>
    
</ul>

export default SignedOutLinks;