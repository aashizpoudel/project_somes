import React,{Component} from 'react';
import {connect} from 'react-redux';
import {signIn} from './../../store/actions/authAction';
import {Redirect} from 'react-router-dom';
 class Login extends Component{
    
    state={
        
        working:false
    };
    

    
    constructor(props){
        super(props);
        
    }

    
    
    render(){
        // console.log(this.props);
        if(this.props.auth.uid){
            return <Redirect to="/" />
        }
        return <div>
                <form onSubmit={(e)=>{
                    // this.props.login({name:"aashiz"});
                e.preventDefault();

                    this.setState({working:true});
                     this.props.signIn({email:e.target.email.value,password:e.target.password.value});
                }}>
                <div className='form-group'>
                    <input className='form-control' name="email" required type="email" placeHolder="Email" />
                </div><div className='form-group'>
                    <input className='form-control' name="password" required type="password" placeHolder="Password" />
                </div><div className='form-group'>
                    <input className='form-control' className='btn btn-primary' type="submit" value="Sign In"/>
                </div>
                </form>
            {this.state.working? <p>Signing in...</p>:''}
        </div>
    }
}

const mapStateToProps=(state)=>{
    return {
        auth : state.firebase.auth
    }
}

const mapDispatchToProps=(dispatch)=>{
    return {
        signIn :credentials=> dispatch(signIn(credentials))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Login);