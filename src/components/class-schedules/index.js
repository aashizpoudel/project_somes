
import React,{Component} from 'react';
import './schedule.css';
// import {db} from './../firebase';
import Schedule from './schedule';

import Adder from './adder';
const days=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];



class Index extends Component{
  
  onDayManipulation(i=1){
    console.log(i);
    if(this.state.day===6 && i===1){
      this.setState({day:0});
      return;
    }
    
    if(this.state.day===0 && i===-1){
      this.setState({day:6});
      return;
    }
    this.setState({day:this.state.day+i})
  }
  
  state={
    department:'IV',
    day:new Date().getDay()
  }
  render(){
    return <div className='border p-3'>
    <div className=''>
    <div className='form-inline justify-content-between'>
    <label>Please select your year and section...</label>
    <select className='form-control px-3' defaultValue={this.state.department} onChange={(v)=>{ this.setState({department:v.target.value})}}>
      <option>I_A/B</option>
      <option>I_C/D</option>
      <option>II</option>
      <option>III</option>
      <option>IV</option>
    </select>
    
    </div>
    <div className='form-inline pt-2 justify-content-between'>
    <a href="#" className="btn" onClick={(e)=>{e.preventDefault();this.onDayManipulation(-1)}}><span className='fa fa-arrow-left'></span></a>
    <span className=''>{days[this.state.day]}</span>
    <a className="btn" onClick={(e)=>{e.preventDefault();this.onDayManipulation(1)}} href="#"><span className='fa fa-arrow-right'></span></a>
    </div>
    </div>
    <hr/>
    <Schedule args={{department_slug:"BME_"+this.state.department,day:this.state.day}}/>
    
    <a className='btn btn-info text-white px-3 mb-2' onClick={()=>{this.setState({adder:!this.state.adder})}}>{this.state.adder? "Close Adder": "Open Adder"}</a>
   {this.state.adder?  

   <Adder args={{department_slug:"BME_"+this.state.department,day:this.state.day}}/> 
   

   : ''}
    </div>
  }
}

export default Index ;