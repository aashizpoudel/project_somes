import React,{Component} from 'react';
import {connect} from 'react-redux';
import './schedule.css';
import {createSchedule} from './../../store/actions/scheduleAction.js';
// import {db} from './../firebase';


const days=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

class Adder extends Component{
  state={
    from:'',
    to:'',
    period:'',
    ...this.props.args
  }
  onClick(e){
    this.props.add({...this.state});
    this.setState({from:this.state.to,to:"",period:""});
  }
  render(){
    return <form className='form-inline' onSubmit={(e)=>{e.preventDefault();this.onClick()
        }}> 
    
        <input className='form-control' required onChange={(e)=>{this.setState({from:e.target.value})}} value={this.state.from} placeholder='from'/>
        <input className='form-control' required onChange={(e)=>{this.setState({to:e.target.value})}} value={this.state.to} placeholder='to'/>
                <input className='form-control' required onChange={(e)=>{this.setState({period:e.target.value})}} value={this.state.period} placeholder='period'/>

        <input type="submit" value="add" className='btn btn-primary btn-small'  />
    
    </form>
  }
}

const mapDispatchToProps= (dispatch)=>({
    add: (payload)=>{
       return dispatch(createSchedule(payload))
    }
})

export default connect(null,mapDispatchToProps)(Adder);