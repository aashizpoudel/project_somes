import React,{Component} from 'react';
import './schedule.css';
import {compose}from 'redux';
import {connect} from 'react-redux';

import {firestoreConnect} from 'react-redux-firebase';
import {deleteSchedule} from './../../store/actions/scheduleAction'



class Schedule extends Component{
    render(){
      
     
        if(this.props.schedules){
          if(this.props.schedules.length == 0){
            return <p>No Schedule Available</p>
          }
        return <div>
            {this.props.schedules.map((e,l)=>{
                return <div key={e.id} className='border shadow my-2 p-3 text-center'>
                           <div><span className='from'>{e.from}</span> - <span className='to'>{e.to}</span></div>
                           <div><span className='period'>{e.period}</span></div>
                            <a onClick={()=>{this.props.delete(e.id)}} title="Delete this schedule" className='btn btn-warning text-white my-2' style={{borderRadius:"50%"}}><i className='fa fa-trash'></i></a>
                        </div>
            })}
           
        </div>
        }
        else
        return <p>Loading...</p>
    }
}

const mapStateToProps = (state)=>{
  return {schedules: state.firestore.ordered.schedules}
}

const mapDispatchToProps = (dispatch)=>{
  return {
    delete:(id)=>dispatch(deleteSchedule(id))
  }
};


export default compose(
  connect(mapStateToProps,mapDispatchToProps),
  firestoreConnect((props)=>{
    return [ {
     collection: "schedules",
      where: [['department_slug', '==', props.args.department_slug],
              ['day', '==', props.args.day]
      ]
    }
      ]
  })
  )(Schedule);
