 var initState={materialError:null};

const materialsReducer = (state=initState,action)=>{
    switch(action.type){
        case "MATERIAL_ERROR":
            return {...state,materialError:action.error};
        case "MATERIAL_SUCCESS":
            return {...state,materialError:null};
    }
    return state;
};
export default materialsReducer;