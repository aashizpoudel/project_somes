import authReducer from './authReducer';
import materialsReducer from './materialsReducer';

import {combineReducers} from 'redux';
import {firebaseReducer} from 'react-redux-firebase';
import {firestoreReducer} from 'redux-firestore';
const rootReducer = combineReducers({
        auth: authReducer,
        materials:materialsReducer,
        firebase:firebaseReducer,
        firestore:firestoreReducer
    });

export default rootReducer;