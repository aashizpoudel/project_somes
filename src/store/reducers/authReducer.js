var initState={
    authError:null
};

const authReducer = (state=initState,action)=>{
    switch(action.type){
        case "LOGIN_ERROR":
            return {
                ...state,authError:"login failed!!"
            }
        case "LOGIN_SUCCESS":
            console.log("login success!!");
            return {
                ...state,authError:null
            }
             case "SIGNOUT_ERROR":
            return {
                ...state,authError:"Sign Out failed!!"
            }
        case "SIGNOUT_SUCCESS":
            console.log("Sign out success!!");
            return {
                ...state,authError:null
            }
        default:
            return state;
    }
};
export default authReducer;