export const createSchedule = (schedule)=>{
    return (dispatch,getState,{getFirestore})=>{
        const firestore = getFirestore();
        
        firestore.collection("schedules").add({...schedule,authFirstName:"Aashiz",authLastName:"Poudel",created_at:new Date()})
        .then((e)=>{
            console.log("Added "+e);
            dispatch({type:"SCHEDULE_SUCCESS"});
        }).catch((err)=>{
            console.log(err);
            dispatch({type:"SCHEDULE_ERROR"});
        })
    }
}

export const deleteSchedule = (id)=>{
    return (dispatch,getState,{getFirestore})=>{
        const firestore = getFirestore();
        
        firestore.collection("schedules").doc(id).delete()
        .then((e)=>{
            console.log("delete "+e);
            dispatch({type:"DELETE_SCHEDULE_SUCCESS"});
        }).catch((err)=>{
            console.log(err);
            dispatch({type:"DELETE_SCHEDULE_ERROR"});
        })
    }
}

