export const createMaterial = (material)=>{
    return (dispatch,getState,{getFirestore})=>{
        const firestore = getFirestore();
        
        firestore.collection("materials").add({...material,authFirstName:"Aashiz",authLastName:"Poudel"})
        .then((e)=>{
            console.log("Added "+e);
            dispatch({type:"MATERIAL_SUCCESS"});
        }).catch((err)=>{
            dispatch({type:"MATERIAL_ERROR"});
        })
    }
}