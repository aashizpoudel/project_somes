import React from 'react';
import ComitteeList from './../components/committee/committeelist';
const Committee = ()=> <div className='p-4'>
    <h3>Our Esteemed Committee</h3>
    <hr/>
    <ComitteeList />
</div>;

export default Committee;