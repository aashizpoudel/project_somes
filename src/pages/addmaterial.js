import React from 'react';
import Login from './../components/auth/signin.js';
import AddMaterial from './../components/materials/add';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
const AddMaterialPage = (props)=>{ 

if(!props.auth.uid){
            return <Redirect to="/login" />
        }

return <div className='row justify-content-center'>
<div className='col-md-10'>
<div className=''>

<AddMaterial history={props.history}/>
</div>
</div>
</div>;
}
const mapStateToProps=(state)=>{
    return {
        auth:state.firebase.auth
    }
}
export default connect(mapStateToProps)(AddMaterialPage);