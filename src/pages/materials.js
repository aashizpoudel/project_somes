import React,{Component} from 'react';
import {Link,Route,Redirect} from 'react-router-dom';
import MaterialsList from './../components/materials/materialslist';
import {firestoreConnect} from 'react-redux-firebase';

import {compose} from 'redux';
import {connect} from 'react-redux';


class  Materials extends Component{
    
    render(){
      
      console.log(this.props.materials);
        if(!this.props.auth.uid){
            return <Redirect to="/login" />
        }
        
        return <div className="border shadow p-3">
                    <div className="row">
                        <div className="col-md-4">
                            <h3 className="mb-sm-2">Material Listing</h3>
                        </div>
                        
                        <div className="col-md-8">
                        <div className="d-flex justify-content-end">
                        <Link  className="btn btn-outline-secondary align-self-center mr-3" to={`${this.props.match.path}/add`} >ADD NEW MATERIAL</Link>

                        <form className='form-inline'>
                        <div className='form-group'>
                            <input type='text' className='form-control' placeholder="Search materials" />
                            <input className='btn btn-primary' type="submit" value="Search" />
                        </div>
                     </form> 

                        

                        </div>
                                             
                        </div>
                    </div>
                    <hr/>
                    
                    <div className='col-md-8'>
                        <div className="wrapper">
                        <p>Showing 4 materials</p>
                        <MaterialsList materials={''} />
                       
                        </div>
                    </div>
                    
                    
                     
                        
                </div>;
    }
}

const mapStateToProps = (state)=>{
    console.log(state.firestore);
    return {
        auth:state.firebase.auth,
        materials:state.firestore.ordered.materials
    }
}
export default compose(
    connect(mapStateToProps),
    firestoreConnect([{collection:"materials",limit:5}])
    )(Materials);
