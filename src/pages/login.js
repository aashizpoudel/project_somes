import React from 'react';
import Login from './../components/auth/signin.js';

const Home = ()=> <div className='row justify-content-center'>
<div className='col-md-5'>
<div className='jumbotron shadow-sm border rounded'>
<h3>Please Sign in for accessing additional features</h3>
<Login/>
</div>
</div>
</div>;

export default Home;