import firebase from 'firebase/app';

import 'firebase/firestore';
import 'firebase/auth';

var config = {
    apiKey: "AIzaSyBHp-XXxf6DeRxNG_7y-oo-ZoidbqEjy4c",
    authDomain: "projectsomes.firebaseapp.com",
    databaseURL: "https://projectsomes.firebaseio.com",
    projectId: "projectsomes",
    storageBucket: "projectsomes.appspot.com",
    messagingSenderId: "503883358247"
  };
  
  firebase.initializeApp(config);
  firebase.firestore().settings({timestampsInSnapshots:true});
  
  export default firebase;